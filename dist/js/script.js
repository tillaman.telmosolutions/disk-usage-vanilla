const folders = document.querySelectorAll(".folder");

folders.forEach((folder) => {
  folder.addEventListener("click", () => {
    folder.classList.toggle("active");
    folder.nextElementSibling.classList.toggle("active");
  });
});
