<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>OVP Panel</title>
    <link rel="stylesheet" href="dist/css/style.css" />
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback" />
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css" />
    <link rel="stylesheet" href="dist/css/adminlte.min.css" />
</head>



<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                        <i class="fas fa-expand-arrows-alt"></i>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->
        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="index3.html" class="brand-link">
                <span class="brand-text font-weight-light">OVP Panel</span>
            </a>
            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                        <li class="nav-item menu-open">
                            <a href="#" class="nav-link active">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>Disk Usage</p>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Disk Usage</h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-3 col-6">
                            <?php
                                        $disk_path = "/";
                                        $disk_total_space = disk_total_space($disk_path);
                                        $disk_free_space = disk_free_space($disk_path);
                                        $disk_total_usage = $disk_total_space-$disk_free_space;
                                        $disk_total_usage_percentage =($disk_total_usage/$disk_total_space)*100;
                                    ?>

                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3><?php echo format_size($disk_total_space); ?></h3>
                                    <p>Disk Total Space</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-bag"></i>
                                </div>

                            </div>
                        </div>

                        <div class="col-lg-3 col-6">

                            <div class="small-box bg-success">
                                <div class="inner">
                                    <h3><?php echo format_size($disk_free_space); ?><sup style="font-size: 20px"></sup>
                                    </h3>
                                    <p>Disk Free Space</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-stats-bars"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-6">

                            <div class="small-box bg-warning">
                                <div class="inner">
                                    <h3><?php echo format_size($disk_total_usage); ?></h3>
                                    <p>Disk Total Usage</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-6">

                            <div class="small-box bg-danger">
                                <div class="inner">
                                    <h3><?php echo number_format($disk_total_usage_percentage,2); ?>%</h3>
                                    <p>Disk Usage %</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-pie-graph"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <?php
                                        $total_size = dir_size($path1,$path1);
                                    ?>
                                <div class="card-header ui-sortable-handle">
                                    <div class="d-flex justify-content-between">
                                        <h3 class="card-title font-weight-bold">Server Details</h3>
                                    </div>
                                </div>
                                <div class="card-body px-0">
                                    <?php
                                        echo '<ul class="directories">';
                                        echo 
                                        '
                                        <li class="file">
                                            <span class="file-name">
                                                SERVER NAME
                                            </span>
                                            <span class="file-size">
                                                '.
                                            $_SERVER['SERVER_NAME']
                                                .'
                                            </span>
                                        </li>

                                        <li class="file">
                                            <span class="file-name">
                                                HOST NAME
                                            </span>
                                            <span class="file-size">
                                                '.
                                               gethostname()
                                                .'
                                            </span>
                                        </li>

                                        <li class="file">
                                            <span class="file-name">
                                                FILE NAME
                                            </span>
                                            <span class="file-size">
                                                '.
                                                $_SERVER['PHP_SELF']
                                                .'
                                            </span>
                                        </li>

                                        
                                        <li class="file">
                                        <span class="file-name">
                                           HTTP HOST
                                        </span>
                                        <span class="file-size">
                                            '.
                                        $_SERVER['HTTP_HOST']
                                            .'
                                        </span>

                                        <li class="file">
                                        <span class="file-name">
                                           HTTP USER AGENT
                                        </span>
                                        <span class="file-size">
                                            '.
                                        $_SERVER['HTTP_USER_AGENT']
                                            .'
                                        </span>
                                        </li>

                                        <li class="file">
                                        <span class="file-name">
                                          SCRIPT NAME
                                        </span>
                                        <span class="file-size">
                                            '.
                                        $_SERVER['SCRIPT_NAME']
                                            .'
                                        </span>
                                        </li>

                                        <li class="file">
                                        <span class="file-name">
                                          SERVER SOFTWARE
                                        </span>
                                        <span class="file-size">
                                            '.
                                        $_SERVER['SERVER_SOFTWARE']
                                            .'
                                        </span>
                                        </li>

                                        <li class="file">
                                        <span class="file-name">
                                          SERVER PORT
                                        </span>
                                        <span class="file-size">
                                            '.
                                        $_SERVER['SERVER_PORT']
                                            .'
                                        </span>
                                        </li>

                                        <li class="file">
                                        <span class="file-name">
                                          Memory usage
                                        </span>
                                        <span class="file-size">
                                            '.
                                            format_size(memory_get_usage())
                                            .'
                                        </span>
                                        </li>
                                        
                                        
                                        
                                        ';
                                        echo '</ul>';
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card ">
                                <?php
                                        $path1 = "/boot/grub";
                                        $path2 = "/boot/grub";
                                        $total_size = dir_size($path2,$path2);
                                    ?>
                                <div class="card-header ui-sortable-handle">
                                    <div class="d-flex justify-content-between">
                                        <h3 class="card-title font-weight-bold">Total Size</h3>
                                        <h3 class="card-title"><?php echo format_size($total_size);?></h3>
                                    </div>
                                </div>
                                <div class="card-body px-0 ">
                                    <?php
                                        function dir_size($directory,$path1) {
                                            $size = 0;
                                            if($directory==$path1."/."||$directory==$path1."/.."){
                                                return -1;
                                            }
                                            if(is_dir($directory)) {
                                                try {
                                                    foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory)) as $file){
                                                        if(is_file($file)){
                                                            $size += $file->getSize();
                                                        }
                                                    }
                                                    return $size;
                                        
                                                } catch (\Throwable $th) {
                                                    echo $th;
                                                    return 0;
                                                }
                                            }
                                            else{
                                                return filesize($directory);
                                            }
                                        }
                                        function format_size($size) {
                                            if($size==-1){
                                                return "--";
                                            }
                                            $mod = 1024;
                                            $units = explode(' ','B KB MB GB TB PB');
                                            for ($i = 0; $size > $mod; $i++) {
                                                $size /= $mod;
                                            }
                                            return round($size, 2) . ' ' . $units[$i];
                                        }
                                   
                                        function listFolderFiles($dir,$path1,$withPercentage=false){
                                            $fileFolderList = scandir($dir);
                                            if(!$withPercentage){
                                                echo '<ul class="folder-target">';
                                                foreach($fileFolderList as $fileFolder){
                                                    if($fileFolder != '.' && $fileFolder != '..'){
                                                        if(!is_dir($dir.'/'.$fileFolder)){
                                                            echo '<li class="file"><span class="file-name">'.$fileFolder.'</span><span class="file-size">'.format_size(dir_size($dir.'/'.$fileFolder,$path1)).'</span>';
                                                        } else {
                                                            echo '<li><div class="folder"><span class="folder-name">'.$fileFolder.'</span><span class="folder-size">'.format_size(dir_size($dir.'/'.$fileFolder,$path1)).'</span></div>';
                                                        }
                                                        if(is_dir($dir.'/'.$fileFolder)) 
                                                        {
                                                            listFolderFiles($dir.'/'.$fileFolder,$path1,$withPercentage);
                                                        }
                                                        echo '</li>';
                                                    }
                                                }
                                                echo '</ul>';
                                            }
                                            else{
                                                $total_size = dir_size($path1,$path1);
                                                echo '<ul class="folder-target">';
                                                foreach($fileFolderList as $fileFolder){
                                                    if($fileFolder != '.' && $fileFolder != '..'){
                                                        $size = dir_size($dir.'/'.$fileFolder,$path1);
                                                        $percentage = ($size/$total_size)*100;
                                                            echo 
                                                            '<li class="file">
                                                                <span class="file-name">'
                                                                .$fileFolder.'
                                                                </span>
                                                                <span class="bar-container">
                                                                    <span style="width:'.$percentage.'%;" class="bar">
                                                                    </span>
                                                                </span>
                                                            </li>';
                                                    }
                                                }
                                                echo '</ul>';
                                            }
                                            
                                        }
                                        echo '<ul class="directories">';
                                        listFolderFiles($path2,$path2,true);
                                        echo '</ul>';

                                    ?>
                                </div>
                            </div>



                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <?php
                                        $total_size = dir_size($path1,$path1);
                                    ?>
                                <div class="card-header ui-sortable-handle">
                                    <div class="d-flex justify-content-between">
                                        <h3 class="card-title font-weight-bold">Total Size</h3>
                                        <h3 class="card-title">
                                            <?php echo format_size($total_size);?></h6>
                                    </div>
                                </div>
                                <div class="card-body px-0">

                                    <?php
                                        echo '<ul class="directories">';
                                        listFolderFiles($path1,$path1);
                                        echo '</ul>';

                                    ?>
                                </div>
                            </div>
                        </div>

                    </div>



                </div>
            </div>
        </div>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>
        <!-- /.control-sidebar -->
        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- To the right -->
            <div class="float-right d-none d-sm-inline"></div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2022
                <a href="https://ovp.gov.ph">OVP Panel</a>.</strong>
            All rights reserved.
        </footer>
    </div>
    <script src="dist/js/script.js"></script>
    <script src="plugins/jquery/jquery.min.js"></script>
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="dist/js/adminlte.min.js"></script>
</body>

</html>